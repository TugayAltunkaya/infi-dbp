package mainPackage;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DBManager {
	
	
	Connection c = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	String comm = "";
	
	public DBManager() throws SQLException
	{
		OpenConnection();
	}
	
	public void OpenConnection() throws SQLException 
	{
		String workingDir = System.getProperty("user.dir");
		try{
			Class.forName("org.sqlite.JDBC");
			c=DriverManager.getConnection(("jdbc:sqlite:"+workingDir+"\\fuho.db"));								
			c.setAutoCommit(false);
		}
		catch (Exception e)
		{
			System.err.println(e.getClass().getName() + ": " + e.getMessage() );
			System.out.println("Verbindung zur Datenbank ist nicht moeglich!");
			System.exit(0);
		}	
	}
	
	public double[] monatsDurchschnitte(String tab) throws SQLException{
		Calendar cal = Calendar.getInstance();
		double[] durchschn = new double[13];
		double anz = 1;
		long start = this.einJahrFrueher();
		long ende = this.monatEnde(start);
		for(int i=0;i<13;i++){
			comm = "select sum( value1 ) from "  + tab
            + " where datecolumn "
            + " between " + start + " and " + ende;
			ps = c.prepareStatement(comm);
			rs = ps.executeQuery();
			while(rs.next()){
				durchschn[i] = rs.getDouble(1);
			}
			comm = "select count(*) from " + tab
	                  + " where datecolumn "
	                  + " between " + start + " and " + ende;
			ps = c.prepareStatement(comm);
			rs = ps.executeQuery();
			while(rs.next()){
				anz = rs.getDouble(1);
			}
			ps.close();
			rs.close();
			if ((anz != 0) && (durchschn[i] != 0)) {
				durchschn[i] = round(durchschn[i] / (100 * anz), 2);
			}
			else durchschn[i] = 0;
			start = this.naechsterMonat(start);
			ende = this.monatEnde(start);
		}
		return durchschn;
	}
	
	public String[] monatsNamen(){
		Calendar cal = Calendar.getInstance();
		String[] monate = new String[13];
		long vorJahr = this.einJahrFrueher();
		cal.setTimeInMillis(vorJahr*1000);
		for(int i=0; i<13;i++){
			monate[i] = new SimpleDateFormat("MMMMMMMMMMM").format(cal.getTime());
			cal.add(Calendar.MONTH, 1);
		}
		return monate;
	}
	
	public String[] minMaxDate(double[] mima, String tab) throws SQLException{
		String[] dates = new String[2];
		Calendar cal = Calendar.getInstance();
		for (int i = 0; i < 2; i++) {
			if (mima[i] != -1111111) {
				comm = "select datecolumn from " + tab + " where value1 = " + mima[i];
				ps = c.prepareStatement(comm);
				rs = ps.executeQuery();
				while(rs.next()){
					cal.setTimeInMillis(rs.getLong(1)*1000);
					dates[i] = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
				}
				
			}else {
				dates[i] = "Not Available";
			}
		}
		ps.close();
		rs.close();
		return dates;
	}
	
	public double[] minMaxVal(String tab) throws SQLException{
		double[] mima = new double[2];
		long start = this.einJahrFrueher();
		comm = "select min(value1) from " + tab + " where datecolumn >= " + start;
		ps = c.prepareStatement(comm);
		rs = ps.executeQuery();
		while(rs.next()) {
			if (rs.getObject(1) != null) {
				mima[0] = rs.getInt(1);
			}else{
				mima[0] = -1111111;
			}
		}
		comm = "select max(value1) from " + tab + " where datecolumn >= " + start;
		ps = c.prepareStatement(comm);
		rs = ps.executeQuery();
		while(rs.next()) {
			if (rs.getObject(1) != null) {
				mima[1] = rs.getInt(1);
			}else{
				mima[0] = -1111111;
			}
		}
		rs.close();
		ps.close();
		
		return mima;
		
	}
	
	private long einJahrFrueher(){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -1);
		Date vorJahr = cal.getTime();
		long time = vorJahr.getTime();
		return time/1000;
	}
	
	private long monatEnde(long timest){
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timest*1000);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date ende = cal.getTime();
		long time = ende.getTime();
		return time/1000;
	}
	
	private long naechsterMonat(long timest){
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(timest*1000);
		cal.add(Calendar.MONTH, 1);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		Date erster = cal.getTime();
		long time = erster.getTime();
		return time/1000;
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}

	
	public void closeConn() throws SQLException
	{
		c.close();
	}

}
