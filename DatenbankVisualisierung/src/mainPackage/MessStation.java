package mainPackage;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MessStation {
	private StringProperty name, minDatum, maxDatum;
	private DoubleProperty minT, maxT;
	
	

	public MessStation(String name, double minT, String minDatum, double maxT, String maxDatum ) {
		super();
		this.name = new SimpleStringProperty(name);
		this.minT = new SimpleDoubleProperty(minT);
		this.minDatum = new SimpleStringProperty(minDatum);
		this.maxT = new SimpleDoubleProperty(maxT);
		this.maxDatum = new SimpleStringProperty(maxDatum);
		
	}
	



	public StringProperty getName() {
		return name;
	}



	public void setName(StringProperty name) {
		this.name = name;
	}



	public StringProperty getMinDatum() {
		return minDatum;
	}



	public void setMinDatum(StringProperty minDatum) {
		this.minDatum = minDatum;
	}



	public StringProperty getMaxDatum() {
		return maxDatum;
	}



	public void setMaxDatum(StringProperty maxDatum) {
		this.maxDatum = maxDatum;
	}



	public DoubleProperty getMinT() {
		return minT;
	}



	public void setMinT(DoubleProperty minT) {
		this.minT = minT;
	}



	public DoubleProperty getMaxT() {
		return maxT;
	}



	public void setMaxT(DoubleProperty maxT) {
		this.maxT = maxT;
	}
	
	

	
	
	

}
