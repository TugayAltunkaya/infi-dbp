package mainPackage;


import java.sql.SQLException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.collections.FXCollections; 
import javafx.collections.ObservableList; 



public class Main extends Application{

	static DBManager dbm;
	static ArrayList<String> mimaTabs;
	static String plotTab;
	static ArrayList<Double> mimaVal;
	static ArrayList<String> mimaDate;
	static String[] monate;
	static double[] dschnitte;
	
	private TableView<MessStation> table = new TableView<MessStation>();
 
    @SuppressWarnings("unchecked")
	@Override public void start(Stage stage) {
    	
    	
    	//Plot
        stage.setTitle("Visualisierung der Messstationen");
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart<String,Number> bc = 
            new BarChart<String,Number>(xAxis,yAxis);
        bc.setTitle("Durchschnittstemperaturen der Messtation");
        xAxis.setLabel("Monat");       
        yAxis.setLabel("Temperatur");
 
        //Daten einfuegen
        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Station: " + plotTab);
        for(int i=0; i<13;i++){
        	series1.getData().add(new XYChart.Data(monate[i], dschnitte[i]));
        }
        
        //Spalten
        TableColumn<MessStation, String> c0 = new TableColumn<MessStation, String>("Station");
        TableColumn<MessStation, Double> c1 = new TableColumn<MessStation, Double>("Minimale Temperatur");
        TableColumn<MessStation, String> c2 = new TableColumn<MessStation, String>("Datum");
        TableColumn<MessStation, Double> c3 = new TableColumn<MessStation, Double>("Maximale Temperatur");
        TableColumn<MessStation, String> c4 = new TableColumn<MessStation, String>("Datum");
        
        c0.prefWidthProperty().bind(table.widthProperty().multiply(0.2));        
        c1.prefWidthProperty().bind(table.widthProperty().multiply(0.2));
        c2.prefWidthProperty().bind(table.widthProperty().multiply(0.2));
        c3.prefWidthProperty().bind(table.widthProperty().multiply(0.2));
        c4.prefWidthProperty().bind(table.widthProperty().multiply(0.2));
        
        /*
         *  c0.setCellValueFactory(
                new Callback<CellDataFeatures<String, String>, ObservableValue<String>>() { 
                    public ObservableValue<String> call( 
                            CellDataFeatures<String, String> m) { 
                        return new SimpleStringProperty(p.getValue()); 
                    } 
                }); 
         */
        //Kuerzer mit lambda ausdr.
        c0.setCellValueFactory((m) -> { 
            return m.getValue().getName();
        });
        c1.setCellValueFactory((m) -> {
        	return m.getValue().getMinT().asObject();
        });
        c2.setCellValueFactory((m) -> {
        	return m.getValue().getMinDatum();
        });
        c3.setCellValueFactory((m) -> {
        	return m.getValue().getMaxT().asObject();
        });
        c4.setCellValueFactory((m) -> {
        	return m.getValue().getMaxDatum();
        });
 
        table.getColumns().addAll(c0, c1,c2,c3,c4);
        table.setItems(createList());
        
        //Layout
        final VBox vbox = new VBox();
        vbox.setSpacing(10);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        
      //Zusammenfügen
        vbox.getChildren().addAll(bc, table);
        
        
        bc.getData().addAll(series1);
        
        Scene scene  = new Scene(vbox);
        
        stage.setScene(scene);
        stage.show();
        
        
    }
    private ObservableList<MessStation> createList() { 
        final ObservableList<MessStation> messSt = FXCollections.observableArrayList();
        for(int i=0;i<5;i++){
        	messSt.add(new MessStation(mimaTabs.get(i), mimaVal.get(i+i),
        			mimaDate.get(i+i), mimaVal.get(i+i+1), mimaDate.get(i+i+1)));
        }
                
        return messSt;
    }
	
	public static void main(String[] args) throws SQLException {
		
		mimaTabs = new ArrayList<String>();
		mimaTabs.add("ID4_DATA");
		mimaTabs.add("ID5_DATA");
		mimaTabs.add("ID6_DATA");
		mimaTabs.add("ID8_DATA");
		mimaTabs.add("ID9_DATA");
		
		plotTab = "ID5_DATA";
		
		try {
			dbm = new DBManager();
			
			//Min Max Werte aus der Datenbank holen und merken
			mimaVal = new ArrayList<Double>();
			mimaDate = new ArrayList<String>();
			for(String tab : mimaTabs){
				double[] mimaV = dbm.minMaxVal(tab);
				String[] mimaD = dbm.minMaxDate(mimaV, tab);
				mimaVal.add(mimaV[0]/100);
				mimaVal.add(mimaV[1]/100);
				mimaDate.add(mimaD[0]);
				mimaDate.add(mimaD[1]);
			}
			
			//Durchschnittswerte fuer die Monate holen und merken
			monate = dbm.monatsNamen();
			dschnitte = dbm.monatsDurchschnitte(plotTab);
			
			//Visualisierung:
			launch(args);
			
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			dbm.closeConn();
		}

	}

}



