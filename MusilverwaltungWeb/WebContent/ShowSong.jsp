<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.sql.*,java.util.*"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body style="background-color: powderblue;">
	<table>
		<%
			Class.forName("org.sqlite.JDBC");
			Connection conn = DriverManager
					.getConnection("jdbc:sqlite:C:/Users/Tugay/Documents/5klasseSWPS/MusilverwaltungWeb/Musikverwaltung.db");
			Statement st = conn.createStatement();

			String song = request.getParameter("Song_Name");
			//ResultSet resultset = 
			// st.executeQuery("select * from Song where Song_Name = '" + song + "'") ; 
			ResultSet resultset = st
					.executeQuery("select * from Song INNER JOIN Album ON Album.Album_ID = Song.Album WHERE Song.Song_Name='"
							+ song + "'");

			if (!resultset.next()) {
				out.println("Song wurde nicht gefunden.");
			} else {
		%>

		<TABLE BORDER="1">
			<TR>
				<TH>ID</TH>
				<TH>Name</TH>
				<TH>Dauer</TH>
				<TH>Erscheinungsdatum</TH>
				<TH>AlbumID</TH>
				<!--                <TH>AlbumID</TH> -->
				<TH>Albumname</TH>
				<TH>Veröffentlichungsdatum</TH>


			</TR>
			<TR>
				<TD><%=resultset.getString(1)%></TD>
				<TD><%=resultset.getString(2)%></TD>
				<TD><%=resultset.getString(3)%></TD>
				<TD><%=resultset.getString(4)%></TD>
				<TD><%=resultset.getString(5)%></TD>
				<%--                <TD> <%= resultset.getString(6) %> </TD> --%>
				<TD><%=resultset.getString(7)%></TD>
				<TD><%=resultset.getString(8)%></TD>
			</TR>
		</TABLE>

		<%
			}
		%>
		<button onclick="goBack()">Startseite</button>

		<script>
			function goBack() {
				window.history.back();
			}
		</script>
</body>
</html>