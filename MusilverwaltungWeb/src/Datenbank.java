import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Datenbank {
	static Statement stmt = null;
	static Connection c = null;
	public static Datenbank db;

	public static void main(String[] args) throws SQLException,
			FileNotFoundException {
		// TODO Auto-generated method stub
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:Musikverwaltung.db");
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql = "CREATE TABLE IF NOT EXISTS Album "
					+ "(Album_ID INTEGER PRIMARY KEY  AUTOINCREMENT   ,"
					+ " Album_Name varchar(100) NOT NULL, "
					+ "Verroeffentlichungsdatum     DATE     NOT NULL);";

			String sql1 = "CREATE TABLE IF NOT EXISTS Song "
					+ "(Song_ID INTEGER PRIMARY KEY  AUTOINCREMENT  ,"
					+ " Song_Name varchar(100) NOT NULL    ,"
					+ " Dauer            float     NOT NULL, "
					+ " Erscheinungsdatum            DATE     NOT NULL, "
					+ " Album INT NOT NULL,"
					+ " FOREIGN KEY(Album) REFERENCES Album(Album_ID));";

			String sql2 = "CREATE TABLE IF NOT EXISTS Musiker_Songs "
					+ "(Musiker INTEGER NOT NULL     ,"
					+ " Song INTEGER NOT NULL     ,"
					+ " FOREIGN KEY(Musiker) REFERENCES Musiker(Musiker_ID),"
					+ " FOREIGN KEY(Song) REFERENCES Song(Song_ID),"
					+ " PRIMARY KEY(Musiker, Song));";

			String sql3 = "CREATE TABLE IF NOT EXISTS Musiker "
					+ "(Musiker_ID INTEGER PRIMARY KEY  AUTOINCREMENT   ,"
					+ " Musiker_Name varchar(100) NOT NULL     ,"
					+ " Geburtsdatum     DATE     NOT NULL, "
					+ " Land varchar(100) NOT NULL);";
			
			String sql4 = "PRAGMA foreign_keys = ON;";
			stmt.executeUpdate(sql4);
			stmt.executeUpdate(sql);
			stmt.execute(sql1);
			stmt.executeUpdate(sql3);
			stmt.executeUpdate(sql2);
			stmt.close();

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Table created successfully");

		String csvFile = "Album.csv";
		String csvFile2 = "Song.CSV";
		String csvFile3 = "Musiker_Songs.CSV";
		String csvFile4 = "Musiker.CSV";
		BufferedReader br = null;
		BufferedReader br2 = null;
		BufferedReader br3 = null;
		BufferedReader br4 = null;
		String line = "";
		String cvsSplitBy = ",";

		try {

			br = new BufferedReader(new FileReader(csvFile));
			br2 = new BufferedReader(new FileReader(csvFile2));
			br3 = new BufferedReader(new FileReader(csvFile3));
			br4 = new BufferedReader(new FileReader(csvFile4));
			while ((line = br.readLine()) != null) {

				String[] Wert = line.split(cvsSplitBy);

				// System.out.print(Wert[0]);
				// System.out.print(Wert[1]);

				PreparedStatement stmt = c
						.prepareStatement("insert into Album values(?,?,?)");
				stmt.setString(1, Wert[0]);
				stmt.setString(2, Wert[1]);
				stmt.setString(3, Wert[2]);
				stmt.executeUpdate();

			}

			while ((line = br2.readLine()) != null) {

				String[] Wert2 = line.split(cvsSplitBy);

				// System.out.print(Wert[0]);
				// System.out.print(Wert[1]);

				PreparedStatement stmt = c
						.prepareStatement("insert into Song values(?,?,?,?,?)");

				stmt.setString(1, Wert2[0]);
				stmt.setString(2, Wert2[1]);
				stmt.setString(3, Wert2[2]);
				stmt.setString(4, Wert2[3]);
				stmt.setString(5, Wert2[4]);
				stmt.executeUpdate();

			}
			while ((line = br4.readLine()) != null) {

				String[] Wert4 = line.split(cvsSplitBy);

				// System.out.print(Wert[0]);
				// System.out.print(Wert[1]);

				PreparedStatement stmt = c
						.prepareStatement("insert into Musiker values(?,?,?,?)");

				stmt.setString(1, Wert4[0]);
				stmt.setString(2, Wert4[1]);
				stmt.setString(3, Wert4[2]);
				stmt.setString(4, Wert4[3]);
				stmt.executeUpdate();

			}

			while ((line = br3.readLine()) != null) {

				String[] Wert3 = line.split(cvsSplitBy);

				// System.out.print(Wert[0]);
				// System.out.print(Wert[1]);

				PreparedStatement stmt = c
						.prepareStatement("insert into Musiker_Songs values(?,?)");

				stmt.setString(1, Wert3[0]);
				stmt.setString(2, Wert3[1]);
				stmt.executeUpdate();

			}

			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (br2 != null) {
				try {
					br2.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (br4 != null) {
				try {
					br4.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (br3 != null) {
				try {
					br3.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		c.close();
	}

}
